# Alkemy Warmup Challenge

El objetivo es crear un pequeño cliente para un blog.

La aplicación muestra una página de login y luego una home que lista los posts. Desde allí se puede acceder al detalle de cada post, editarlo o eliminarlo. Además, desde una barra de navegación se puede acceder a un formulario para crear un post y volver a la home.

# ¿Cómo ejecutarlo?

Para correrlo en modo desarrollo es necesario clonar el repositorio, instalar las dependencias con `npm install` y luego ejecutar `npm run dev`.

# Tecnologías y herramientas utilizadas

- React
- React Bootstrap
- Bootstrap
- ViteJS
- Eslint
- Prettier

# Puntos a mejorar

- La eliminación de un post no es comunicada al usuario, solo se ve que se elimina.
- La creación de un nuevo post o la edición de uno existente solo son confirmadas en la consola, donde se ve la respuesta devuela por la API.
- Mejorar la interfaz.
- Implementar paginación para el listado de posts.
