import * as React from 'react';
import { Navbar, Nav } from 'react-bootstrap';
import { Link } from 'react-router-dom';

function CustomNavbar() {
  return (
    <Navbar bg='dark' className='p-3' variant='dark'>
      <Navbar.Brand as={Link} to='/'>
        Blog CMS
      </Navbar.Brand>
      <Nav>
        <Nav.Link as={Link} to='/'>
          Home
        </Nav.Link>
        <Nav.Link as={Link} to='/create'>
          Crear post
        </Nav.Link>
      </Nav>
    </Navbar>
  );
}

export default CustomNavbar;
