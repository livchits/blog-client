import * as React from 'react';
import PropTypes from 'prop-types';
import { Field, Form, Formik, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import { FormGroup, FormLabel, FormControl, Button } from 'react-bootstrap';

import api from '../services/api';

function PostForm({ post = { title: '', body: '' } }) {
  const { title, body, id, userId } = post;
  const formAction = title && body ? 'editPost' : 'createPost';

  return (
    <Formik
      initialValues={{ title, body }}
      validationSchema={Yup.object({
        title: Yup.string().required('Es necesario un título para el post.'),
        body: Yup.string().required('El contenido del post no puede estar vacío.'),
      })}
      onSubmit={(values) => api[formAction]({ id, userId, ...values })}
    >
      {({ errors }) => (
        <Form>
          <div className='position-relative'>
            <FormLabel htmlFor='title'>Título:</FormLabel>
            <Field
              as={FormControl}
              className={errors.title && 'is-invalid'}
              id='title'
              name='title'
              placeholder='Ingrese un título para el post'
              type='text'
            />
            <ErrorMessage name='title'>
              {(msg) => <div className='invalid-tooltip mt-1'>{msg}</div>}
            </ErrorMessage>
          </div>
          <Field name='body'>
            {({ field: { value, onChange, onBlur } }) => (
              <FormGroup className='mt-4 position-relative' controlId='body'>
                <FormLabel>Contenido:</FormLabel>
                <FormControl
                  as='textarea'
                  className={errors.body && 'is-invalid'}
                  placeholder='Coloque aquí el texto'
                  rows={8}
                  value={value}
                  onBlur={onBlur}
                  onChange={onChange}
                />
                <ErrorMessage name='body'>
                  {(msg) => <div className='invalid-tooltip mt-1'>{msg}</div>}
                </ErrorMessage>
              </FormGroup>
            )}
          </Field>
          <Button
            className='mt-3 d-block mx-auto'
            size='lg'
            type='submit'
            variant='primary'
          >
            {formAction === 'createPost' ? 'Crear ' : 'Guardar '} post
          </Button>
        </Form>
      )}
    </Formik>
  );
}

PostForm.propTypes = { post: PropTypes.object };

export default PostForm;
