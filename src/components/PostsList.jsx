import * as React from 'react';
import PropTypes from 'prop-types';
import { Container, ListGroup, Row, Col, ButtonGroup, Button } from 'react-bootstrap';
import { Link, useRouteMatch } from 'react-router-dom';

import api from '../services/api';
import deletePostWithId from '../utils/deletePostWithId';

function PostsList({ posts, setPosts }) {
  const { url } = useRouteMatch();

  const handleDelete = (idToDelete) => {
    api.deletePost(idToDelete).then((response) => {
      if (response.ok) {
        setPosts((previousPosts) => ({
          ...previousPosts,
          data: deletePostWithId(idToDelete, posts),
        }));
      }
    });
  };

  return (
    <>
      <h1>Listado de posts</h1>
      <ListGroup as='ul'>
        {posts.map(({ id, title }) => (
          <ListGroup.Item key={id} as='li'>
            <Container>
              <Row>
                <Col>
                  <p>{title}</p>
                </Col>
                <Col>
                  <ButtonGroup className='w-75 ms-auto d-sm-block'>
                    <Button as={Link} to={`${url}/${id}`} variant='outline-primary'>
                      Detalle
                    </Button>
                    <Button as={Link} to={`${url}/edit/${id}`} variant='outline-primary'>
                      Editar
                    </Button>
                    <Button variant='outline-danger' onClick={() => handleDelete(id)}>
                      Eliminar
                    </Button>
                  </ButtonGroup>
                </Col>
              </Row>
            </Container>
          </ListGroup.Item>
        ))}
      </ListGroup>
    </>
  );
}

PostsList.propTypes = {
  posts: PropTypes.array.isRequired,
  setPosts: PropTypes.func.isRequired,
};

export default PostsList;
