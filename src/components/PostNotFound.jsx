import * as React from 'react';
import PropTypes from 'prop-types';
import { Alert } from 'react-bootstrap';

function PostNotFound({ postId }) {
  return (
    <Alert variant='danger'>
      <Alert.Heading>Ups... Post no encontrado.</Alert.Heading>
      <p>No existe el post con el ID: {postId}</p>
    </Alert>
  );
}

PostNotFound.propTypes = { postId: PropTypes.string.isRequired };

export default PostNotFound;
