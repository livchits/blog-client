function findPostById(postId, posts) {
  const searchedPost = posts.find(({ id }) => id === Number(postId));
  return searchedPost;
}

export default findPostById;
