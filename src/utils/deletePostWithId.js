function deletePostWithId(idToDelete, posts) {
  return posts.filter(({ id }) => id !== idToDelete);
}

export default deletePostWithId;
