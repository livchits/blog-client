import * as React from 'react';

import { POSTS_URL } from '../constants';

function useGetPosts() {
  const [{ status, data: posts, error }, setPosts] = React.useState({
    status: 'idle',
    data: null,
    error: null,
  });

  React.useEffect(() => {
    setPosts((state) => ({ ...state, status: 'pending' }));
    fetch(POSTS_URL)
      .then((response) => response.json())
      .then((data) => {
        setPosts((state) => ({ ...state, data, status: 'resolved' }));
      })
      .catch((error) => {
        setPosts((state) => ({
          ...state,
          status: 'rejected',
          error: error.message,
        }));
      });
  }, []);

  return { status, posts, error, setPosts };
}

export default useGetPosts;
