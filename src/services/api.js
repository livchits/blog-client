import { POSTS_URL } from '../constants';

export default {
  createPost: ({ title, body }) =>
    fetch(POSTS_URL, {
      method: 'POST',
      body: JSON.stringify({
        title,
        body,
      }),
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
      },
    })
      .then((response) => response.json())
      .then((json) => console.log(json))
      .catch((error) => console.error(error.message)),
  deletePost: (id) =>
    fetch(`${POSTS_URL}/${id}`, {
      method: 'DELETE',
    }),
  editPost: ({ id, userId, title, body }) =>
    fetch(`${POSTS_URL}/${id}`, {
      method: 'PUT',
      body: JSON.stringify({
        id,
        title,
        body,
        userId,
      }),
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
      },
    })
      .then((response) => response.json())
      .then((json) => console.log(json)),
};
