import * as React from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import { UserProvider } from './context/UserContext';
import PrivateRoute from './components/PrivateRoute';
import Home from './pages/Home';
import Login from './pages/Login';

function App() {
  return (
    <UserProvider>
      <Router>
        <Container>
          <Switch>
            <Route exact path='/login'>
              <Login />
            </Route>
            <PrivateRoute path='/'>
              <Home />
            </PrivateRoute>
          </Switch>
        </Container>
      </Router>
    </UserProvider>
  );
}

export default App;
