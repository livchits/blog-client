import * as React from 'react';
import PropTypes from 'prop-types';
import { Card } from 'react-bootstrap';

function PostDetails({ post }) {
  const detailsTextAndOrder = {
    userId: { text: 'ID del usuario', order: 4 },
    id: { text: 'ID', order: 3 },
    title: { text: 'Título', order: 1 },
    body: { text: 'Contenido', order: 2 },
  };

  return (
    <Card bg='light' border='light'>
      <Card.Header as='h1'>Detalles del post</Card.Header>
      <Card.Body>
        {Object.entries(post)
          .sort(
            (detail, nextDetail) =>
              detailsTextAndOrder[detail[0]].order -
              detailsTextAndOrder[nextDetail[0]].order
          )
          .map(([detailProperty, detailValue]) => (
            <p key={detailProperty}>
              <span className='fw-bold fs-5'>
                {detailsTextAndOrder[detailProperty].text}:{' '}
              </span>
              <span>{detailValue}</span>
            </p>
          ))}
      </Card.Body>
    </Card>
  );
}

PostDetails.propTypes = { post: PropTypes.object.isRequired };

export default PostDetails;
