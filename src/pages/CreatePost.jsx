import * as React from 'react';

import PostForm from '../components/PostForm';

function CreatePost() {
  return (
    <>
      <h1 className='my-3'>Crear nuevo post</h1>
      <PostForm />
    </>
  );
}

export default CreatePost;
