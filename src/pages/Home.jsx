import * as React from 'react';
import { Alert, Spinner, Row } from 'react-bootstrap';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';

import PostNotFound from '../components/PostNotFound';
import PostsList from '../components/PostsList';
import CustomNavbar from '../components/CustomNavbar';
import useGetPosts from '../hooks/useGetPosts';
import findPostById from '../utils/findPostById';

import CreatePost from './CreatePost';
import PostDetails from './PostDetails';
import EditPost from './EditPost';

function Home() {
  const { status, posts, error, setPosts } = useGetPosts();

  if (status === 'resolved') {
    return (
      <Router>
        <CustomNavbar />
        <Switch>
          <Route exact path='/'>
            <Redirect to='/posts' />
          </Route>
          <Route exact path='/posts'>
            <PostsList posts={posts} setPosts={setPosts} />
          </Route>
          <Route
            exact
            path='/posts/:postId'
            render={({ match }) => {
              const { postId } = match.params;
              const post = findPostById(postId, posts);
              return post ? (
                <PostDetails post={post} />
              ) : (
                <PostNotFound postId={postId} />
              );
            }}
          />
          <Route
            exact
            path='/posts/edit/:postId'
            render={({ match }) => {
              const { postId } = match.params;
              const post = findPostById(postId, posts);
              return post ? <EditPost post={post} /> : <PostNotFound postId={postId} />;
            }}
          />
          <Route exact path='/create'>
            <CreatePost />
          </Route>
        </Switch>
      </Router>
    );
  }

  if (error) {
    return (
      <Row className='align-items-center vh-100 justify-content-center'>
        <Alert className='w-75 text-center fs-3 fw-bold' variant='danger'>
          Ups... algo salió mal tratando de recuperar los posts.
        </Alert>
      </Row>
    );
  }

  return (
    <Row className='align-items-center vh-100 justify-content-center'>
      <Spinner animation='border' role='status' />
    </Row>
  );
}

export default Home;
