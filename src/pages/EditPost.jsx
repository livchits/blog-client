import * as React from 'react';
import PropTypes from 'prop-types';

import PostForm from '../components/PostForm';

function EditPost({ post }) {
  return (
    <>
      <h1 className='my-3'>Editar post</h1>
      <PostForm post={post} />
    </>
  );
}

EditPost.propTypes = { post: PropTypes.object.isRequired };

export default EditPost;
