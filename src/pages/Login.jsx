import * as React from 'react';
import { Field, Form, Formik, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import { FormLabel, FormControl, Button, Row, Alert, Spinner } from 'react-bootstrap';
import { Redirect } from 'react-router-dom';

import useLogin from '../hooks/useLogin';
import { useUser } from '../context/UserContext';

function Login() {
  const [formData, setFormData] = React.useState(null);
  const [user] = useUser();
  const { status, error } = useLogin(formData);

  const handleSubmit = ({ email, password }) => {
    const formData = new FormData();
    formData.append('email', email);
    formData.append('password', password);
    setFormData(formData);
  };

  return user ? (
    <Redirect to='/' />
  ) : (
    <Row className='vh-100 bg-light'>
      <Row className='px-2 mx-auto' style={{ maxWidth: '500px' }}>
        <h1 className='text-center align-self-start mt-5 pt-5 fw-bold fs-1'>
          Bienvenido al CMS
        </h1>
        <Formik
          initialValues={{ email: '', password: '' }}
          validationSchema={Yup.object({
            email: Yup.string().required('El email no puede estar vacío.'),
            password: Yup.string().required('El password no puede estar vacío.'),
          })}
          onSubmit={handleSubmit}
        >
          <Form>
            <FormLabel htmlFor='email'>Email:</FormLabel>
            <Field
              as={FormControl}
              className='mb-2'
              id='email'
              name='email'
              placeholder='challenge@alkemy.org'
              type='email'
            />
            <ErrorMessage name='email'>
              {(msg) => <Alert variant='danger'>{msg}</Alert>}
            </ErrorMessage>
            <FormLabel className='d-block pt-4' htmlFor='password'>
              Password:
            </FormLabel>
            <Field
              as={FormControl}
              className='mb-2'
              id='password'
              name='password'
              placeholder='react'
              type='password'
            />
            <ErrorMessage name='password'>
              {(msg) => <Alert variant='danger'>{msg}</Alert>}
            </ErrorMessage>
            <Button
              className='mt-5 d-block mx-auto'
              disabled={status === 'pending'}
              size='lg'
              style={{ height: '55px', width: '115px' }}
              type='submit'
              variant='primary'
            >
              {status === 'pending' ? (
                <Spinner animation='border' as='span' variant='light' />
              ) : (
                'Enviar'
              )}
            </Button>
            {error.error && (
              <Alert className='mt-3' variant='danger'>
                {error.message}
              </Alert>
            )}
          </Form>
        </Formik>
      </Row>
    </Row>
  );
}

export default Login;
